ARG HUGOVERSION=0.111.3

FROM klakegg/hugo:${HUGOVERSION}-ext-ubuntu-ci

RUN apt-get update && apt-get install curl -y
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get update && apt-get install git-lfs ssh -y --no-install-recommends
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
RUN mkdir /root/.ssh && ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
